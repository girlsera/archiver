package encode;

import IO.ReadFile;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ConvertCharsToBinaryCode {
    public static List<String> getCode(String text) {
        List<String> result = new ArrayList<>();
        Map<String, String> map = ReadFile.readTable("src/output.txt");
        int k = text.length() / 100 + (text.length() % 100 == 0 ? 0 : 1);
        for (int i = 0; i < k; i++) {
            int end = i * 100 + 100 < text.length() ? i * 100 + 100 : text.length();
            char[] chars = text.substring(i * 100, end).toCharArray();
            StringBuilder sb = new StringBuilder();
            for (char ch : chars) {
                sb.append(map.get(Character.toString(ch)));
            }
            result.add(sb.toString());
        }
        return result;
    }
}
