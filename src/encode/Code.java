package encode;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import IO.*;

/**
 * @author Regina
 *         11-602
 */
public class Code {
    public static String compress(String file) throws FileNotFoundException {
        String file_output = "src\\output.txt";
        char[] input = ReadFile.read(file).toCharArray();
        Scanner sc = new Scanner(new File(file_output));
        String code = "";

        int i = 0;
        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            for (char c : input) {
                if (line.charAt(0) == c) {
                    code += line.substring(2, line.length());
                    i++;
                }
            }
        }
        return code;
    }
}
