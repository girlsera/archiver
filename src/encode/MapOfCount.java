package encode;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Elina Batyrova
 *         11-602
 *         Возвращаем Map <Символ, количество его вхождений в строку>
 */
public class MapOfCount {

    public static Map<Character, Integer> countOfPopularLetters(String str) {
        String text = str;
        List<Character> textInChar;
        textInChar = text.chars().mapToObj(e -> (char) e).filter(e -> !e.equals("")).collect(Collectors.toList());
        Map<Character, Integer> countChar = textInChar.stream().collect(HashMap::new, (m, c) -> {
            m.put(c, m.containsKey(c) ? (m.get(c) + 1) : 1);
        }, HashMap::putAll);
        return countChar;
    }
}
