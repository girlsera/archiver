package encode;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class ConvertToBytesTest {

    @Test
    public void getBytes() throws Exception {
        byte[] expected = {-30, 104};
        assertArrayEquals(expected, ConvertToBytes.getBytes("1110001001101"));
    }


    @Test
    public void adjustTheLengthTest() throws Exception {
        assertEquals("11000000", ConvertToBytes.adjustTheLength("11"));
    }

    @Test
    public void toDecimalTest() throws Exception {
        assertEquals(-128, ConvertToBytes.toDecimal("10000000"));
    }

    @Test
    public void invertTest() throws Exception {
        assertEquals("1011100", ConvertToBytes.invert("0100011"));
    }

    @Test
    public void add1Test() throws Exception {
        assertEquals("10000", ConvertToBytes.add1("1111"));
    }

    @Test
    public void add1Test2() throws Exception {
        assertEquals("1011100", ConvertToBytes.add1("1011011"));
    }

}