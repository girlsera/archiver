package encode;

import IO.ReadFile;

import java.util.ArrayList;
import java.util.List;

public class ConvertToBytes {

    final static int lengthOfByte = 8;

    public static byte[] getBytes(String string) {
        string = adjustTheLength(string);
        byte[] result = new byte[string.length() / lengthOfByte];
        for (int i = 0; i < string.length() / lengthOfByte; i++) {
            result[i] = toDecimal(string.substring(i * lengthOfByte, i * lengthOfByte + lengthOfByte));
        }
        return result;
    }

    public static String adjustTheLength(String s) {
        StringBuilder stringBuilder = new StringBuilder();
        if (s.length() % lengthOfByte == 0) return s;
        else stringBuilder.append(s);
        for (int i = 0; i < lengthOfByte - s.length() % lengthOfByte; i++) {
            stringBuilder.append("0");
        }
        return stringBuilder.toString();
    }

    public static List<String> adjustTheLength(List<String> s) {
        StringBuilder stringBuilder = new StringBuilder();
        long length = s.stream().mapToInt(i -> i.length()).sum();
        System.out.println(length);
        System.out.println();
        if (length % lengthOfByte == 0) return s;
        else stringBuilder.append(s.get(s.size() - 1));
        for (int i = 0; i < lengthOfByte - length % lengthOfByte; i++) {
            stringBuilder.append("0");
        }
        s.remove(s.size() - 1);
        s.add(stringBuilder.toString());
        return s;
    }

    public static byte toDecimal(String number) {
        byte res = 0;
        byte k = 1;
        boolean isNegative = false;
        if (number.charAt(0) == '1') {
            isNegative = true;
            number = invert(number);
            number = add1(number);
        }
        for (int i = number.length() - 1; i >= 0; i--, k *= 2) {
            res += (number.charAt(i) - '0') * k;
        }
        if (isNegative) res *= -1;
        return res;
    }

    public static String invert(String string) {
        StringBuilder res = new StringBuilder();
        for (int i = 0; i < string.length(); i++) {
            res.append(string.charAt(i) == '0' ? "1" : "0");
        }
        return res.toString();
    }

    public static String add1(String number) {
        StringBuilder stringBuilder = new StringBuilder();
        boolean hasRemainder = true;
        for (int i = number.length() - 1; i >= 0 && hasRemainder; i--) {
            if (number.charAt(i) == '0') {
                stringBuilder.append('1');
                hasRemainder = false;
            } else {
                stringBuilder.append('0');
            }
        }
        if (hasRemainder) stringBuilder.append("1");
        else {
            for (int i = number.length() - stringBuilder.length() - 1; i >= 0; i--) {
                stringBuilder.append(number.charAt(i));
            }
        }
        return stringBuilder.reverse().toString();
    }

    public static List<Byte> convertTextToBytes(String name, List<String> code, String fileName) {
        String end = code.get(code.size() - 1) + ReadFile.readTable("src/output.txt").get("end");
        code.remove(code.get(code.size() - 1));
        code.add(end);
        adjustTheLength(code);
        String remained = "";
        List<Byte> bytes = new ArrayList<>();
        //////кодируем имя
        for (byte i : name.getBytes()) {
            bytes.add(i);
        }
        bytes.add((byte) 127);
        bytes.add((byte) 127);
        bytes.add((byte) 127);
        ////////

        /////кодируем весь используемый алфавит
        StringBuilder sb = new StringBuilder();
        for (String s : GettingCharsFromFile.getChars(fileName)) {
            sb.append(s);
        }
        String alphabet = sb.substring(0, sb.length() - 3);//убираем служебный символ end
        for (byte i : alphabet.getBytes()) {
            bytes.add(i);
        }
        bytes.add((byte) 127);
        bytes.add((byte) 127);
        bytes.add((byte) 127);
        /////////////////////

        for (int i = 0; i < code.size(); i++) {
            bytes.add(toDecimal(remained + code.get(i).substring(0, 8 - remained.length())));
            int j = 8 - remained.length();
            int k = 0;
            while (j + k * 8 + 8 < code.get(i).length()) {
                bytes.add(toDecimal(code.get(i).substring(j + k * 8, j + k * 8 + 8)));
                k++;
            }
            remained = code.get(i).substring(j + k * 8, code.get(i).length());
        }
        if (remained.length() > 0) bytes.add(toDecimal(remained));

        return bytes;
    }
}
