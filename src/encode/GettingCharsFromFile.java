package encode;

import IO.ReadFile;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

public class GettingCharsFromFile {
    public static List<String> getChars(String file) {
        String s = ReadFile.read(file);
        Map<Character, Integer> map = MapOfCount.countOfPopularLetters(s);
        List<String> chars = new ArrayList<>();
        System.out.println(map);
        map.entrySet()
                .stream()
                .sorted(new FrequencyEntryComparator())
                .forEach(i -> chars.add(i.getKey().toString()));
        chars.add("end");
        return chars;
    }

    public static class FrequencyEntryComparator implements Comparator<Map.Entry<Character, Integer>> {
        @Override
        public int compare(Map.Entry<Character, Integer> o1, Map.Entry<Character, Integer> o2) {
            return o1.getValue().compareTo(o2.getValue());
        }
    }
}
