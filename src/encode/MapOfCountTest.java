package encode;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Elina Batyrova
 *         Тесты класса MapOfCountt
 */
public class MapOfCountTest {

    @Test
    public void countOfPopularLettersTest() {
        MapOfCount c = new MapOfCount();
        Map<Character, Integer> actual = c.countOfPopularLetters("Aasddd");
        Map<Character, Integer> expecteds = new HashMap<>();
        expecteds.put('A', 1);
        expecteds.put('a', 1);
        expecteds.put('s', 1);
        expecteds.put('d', 3);
        Assert.assertEquals(actual, expecteds);
    }
}
