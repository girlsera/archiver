package decode;

import IO.WriteFile;
import tree.*;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

public class Decode {
    public static void decode(List<Byte> bytes) {
        String name = getStringFromBytes(0, bytes).replace("\000", "");
        String alphabet = getStringFromBytes(name.getBytes().length + 3, bytes).replace("\000", "");
        bytes = bytes.subList(name.getBytes().length + alphabet.getBytes().length + 6, bytes.size());
        List<String> alphabetList = new ArrayList<>();
        for (char c : alphabet.toCharArray()) {
            alphabetList.add(Character.toString(c));
        }
        alphabetList.add("end");
        INode node = Tree.getTree(alphabetList);
        List<String> code = new ArrayList<>();
        for (int i = 0; i < bytes.size() / 20; i++) {
            int end = i * 20 + 20 <= bytes.size() ? i * 20 + 20 : bytes.size();
            StringBuilder sb = new StringBuilder();
            for (int j = i * 20; j < end; j++) {
                sb.append(Binary.add(bytes.get(j)));
            }
            code.add(sb.toString());
        }
        System.out.println(code);
        String content = DecodeText.decodeText(node, code);
        WriteFile.writeNewFile("output/" + name(name), content);
    }

    /**
     * @author Regina
     */
    public static String name(String fileName) {
        String file = fileName.substring(fileName.lastIndexOf('\\') + 1);
        return file;
    }

    public static String getStringFromBytes(int begin, List<Byte> bytes) {
        ByteBuffer bb = ByteBuffer.allocate(1024);
        int separator = 127;
        int maxCount = 3;
        boolean flag = false;
        int i = begin;
        int count = 0;
        while (i < bytes.size() && !flag) {
            if (bytes.get(i) != separator) bb.put(i - begin, bytes.get(i));
            else {
                count++;
                if (count < maxCount) bb.put(i - begin, bytes.get(i));
                else {
                    flag = true;
                    count = 0;
                }
            }
            i++;
        }
        String res = Charset.forName("UTF-8").decode(bb).toString().replace("\000", "");
        return res.substring(0, res.length() - 2);
    }
}
