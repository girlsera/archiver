package decode;

import tree.INode;

import java.util.List;

public class DecodeText {
    public static String decodeText(INode tree, List<String> code) {
        INode currentNode = tree;
        StringBuilder text = new StringBuilder();
        for (String s : code) {
            int i = 0;
            while (i < s.length()) {
                currentNode = move(s.charAt(i), currentNode);
                if (currentNode.getLeftChild() == null && currentNode.getRightChild() == null) {
                    if (!currentNode.getValue().equals("end")) {
                        text.append(currentNode.getValue());
                        currentNode = tree;
                    } else return text.toString();
                }
                i++;
            }
        }
        return text.toString();
    }

    public static INode move(char i, INode node) {
        if (i == '0') {
            if (node.getLeftChild() != null) node = node.getLeftChild();
        } else {
            if (node.getRightChild() != null) node = node.getRightChild();
        }
        return node;
    }
}
