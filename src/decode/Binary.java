package decode;

/**
 * @author Regina
 *         11-602
 */
public class Binary {
    public static String sub(int a) {
        a = a + 0b1;
        String binString = Integer.toBinaryString(a);
        while (binString.length() < 8) {
            binString = "0" + binString;
        }
        return binString;
    }

    public static String add(int a) {
        String bin; // десятичное число с добавленными нулями
        String binaryNum = ""; //конвертированное число

        if (a < 0) {
            a *= -1;
            bin = convert(a);
            for (int i = 0; i < bin.length(); i++) {
                if (bin.charAt(i) == '0') {
                    binaryNum += 1;
                } else
                    binaryNum += 0;
            }
            a = 1 + Integer.parseInt(binaryNum, 2);
        } else {
            bin = convert(a);
            return bin;
        }
        return Integer.toBinaryString(a);
    }

    public static String convert(int a) {
        String binString = Integer.toBinaryString(a);
        while (binString.length() < 8) {
            binString = "0" + binString;
        }
        return binString;
    }
}
