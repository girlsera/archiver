package tree;

import tree.INode;
import tree.Node;

import java.util.ArrayList;
import java.util.List;

public class Tree {
    public static INode getTree(List<String> chars) {
        List<INode> nodes = new ArrayList<>();
        chars.forEach(i -> {
            Node node = new Node(i, nodes.size());
            nodes.add(node);
            System.out.println(node);
        });

        while (nodes.size() > 1) {
            INode node1 = nodes.get(0);
            nodes.remove(0);
            INode node2 = nodes.get(0);
            nodes.remove(0);
            Node newNode = new Node(node1.getPriority() + node2.getPriority());
            newNode.setLeftChild(node1);
            newNode.setRightChild(node2);
            int i = 0;
            while (i < nodes.size() && nodes.get(i).getPriority() < newNode.getPriority())
                i++;
            if (i < nodes.size()) nodes.add(i, newNode);
            else nodes.add(newNode);
        }
        return nodes.get(0);
    }

}
