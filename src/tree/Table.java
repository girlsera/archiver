package tree;

import IO.WriteFile;
import encode.GettingCharsFromFile;

/**
 * @author Elina Batyrova
 *         11-602
 *         Обход дерева, представление через 0 и 1, построение таблички
 */
public class Table {

    public String[] buildingTable(INode node) {
        String[] codeTable = new String[256];
        table(node, new StringBuilder(), codeTable);
        return codeTable;
    }

    private void table(INode node, StringBuilder code, String[] table) {
        if (node.getLeftChild() != null) {
            table(node.getLeftChild(), code.append('0'), table);
            code.deleteCharAt(code.length() - 1);
        }
        if (node.getRightChild() != null) {
            table(node.getRightChild(), code.append('1'), table);
            code.deleteCharAt(code.length() - 1);
        }
    }

    public void printTable(INode node, String fileOut) {
        print(node, new StringBuilder(), fileOut);
    }

    private void print(INode current, StringBuilder code, String fileOut) {
        if (current.getLeftChild() != null) {
            print(current.getLeftChild(), code.append('0'), fileOut);
            code.deleteCharAt(code.length() - 1);
        }
        if (current.getRightChild() != null) {
            print(current.getRightChild(), code.append('1'), fileOut);
            code.deleteCharAt(code.length() - 1);
        }
        if (current.getRightChild() == null && current.getLeftChild() == null) {
            WriteFile.writeIntoFile(fileOut, current.getValue() + "\t" + code);
        }
    }
}
