package tree;

import java.util.Comparator;

public class Node implements INode {
    INode left;
    INode right;
    INode root;
    long priority;
    String value = "☐";

    public Node(String c, long priority) {
        value = c;
        this.priority = priority;
    }

    public Node(long priority) {
        this.priority = priority;
    }

    @Override
    public long getPriority() {
        return priority;
    }

    @Override
    public INode getLeftChild() {
        return left;
    }

    @Override
    public INode getRightChild() {
        return right;
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public INode getRoot() {
        return root;
    }

    @Override
    public void setPriority(int priority) {
        this.priority = priority;
    }

    @Override
    public void setLeftChild(INode node) {
        node.setRoot(this);
        left = node;
    }

    public void setRightChild(INode right) {
        right.setRoot(this);
        this.right = right;
    }

    @Override
    public void setRoot(INode root) {
        this.root = root;
    }

    @Override
    public String toString() {
        return "[" + left + " || " + value + " ||" + right + "]";
    }
}

class NodeComparator implements Comparator<INode> {
    @Override
    public int compare(INode o1, INode o2) {
        return o1.getPriority() > o2.getPriority() ? 1 : (o1.getPriority() == o2.getPriority() ? 0 : -1);
    }
}