package tree;

public interface INode {
    INode getLeftChild();

    INode getRightChild();

    INode getRoot();

    String getValue();

    long getPriority();

    void setPriority(int priority);

    void setLeftChild(INode node);

    void setRightChild(INode node);

    void setRoot(INode node);
}
