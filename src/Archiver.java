import IO.ReadFile;
import IO.WriteFile;
import decode.Decode;
import encode.ConvertToBytes;
import encode.GettingCharsFromFile;
import tree.INode;
import tree.Table;
import tree.Tree;

import java.io.File;
import java.util.List;
/**
 *     Главный класс Archiver
 */

import static encode.ConvertCharsToBinaryCode.getCode;

public class Archiver {
    public static void compress(File file) throws Exception {
        if (file.getName().substring(file.getName().length() - 3, file.getName().length()).equals("txt")) {
            makeTable(file.getAbsolutePath());
            String text = ReadFile.read(file.getAbsolutePath());
            List<String> code = getCode(text);
            List<Byte> bytes = ConvertToBytes.convertTextToBytes(file.getAbsolutePath(), code, file.getAbsolutePath());
            WriteFile.writeNewFile("archived.arc", bytes);
            WriteFile.clearFile("src/output.txt");
        } else throw new Exception("Unsupported file extension!");
    }

    public static void decompress(File file) throws Exception {
        if (file.getName().substring(file.getName().length() - 3, file.getName().length()).equals("arc")) {
            Decode.decode(ReadFile.readBytes(file.getAbsolutePath()));
        } else throw new Exception("Unsupported file extension!");
    }

    public static void makeTable(String file) {
        INode node = Tree.getTree(GettingCharsFromFile.getChars(file));
        Table t = new Table();
        t.buildingTable(node);
        t.printTable(node, "src/output.txt");
    }

    public static void main(String[] args) throws Exception {
        compress(new File("src/file.txt"));
        decompress(new File("archived.arc"));
    }
}
