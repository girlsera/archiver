package IO;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.List;

/**
 * @author Elina Batyrova
 *         11-602
 *         Запись в файл
 */
public class WriteFile {

    public static void writeIntoFile(String fileName, String string) {
        try {
            FileWriter fileWriter = new FileWriter(new File(fileName), true);
            fileWriter.write(string + "\r\n");
            fileWriter.close();
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    public static void writeNewFile(String outputFile, List<Byte> bytes) {
        try {
            RandomAccessFile file = new RandomAccessFile(new File(outputFile), "rw");
            bytes.forEach(i -> {
                try {
                    file.write(i);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void writeNewFile(String outputFile, String content) {
        try {
            File file = new File(outputFile);
            file.createNewFile();
            FileChannel fc = new FileOutputStream(outputFile).getChannel();
            fc.write(ByteBuffer.wrap(content.getBytes()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void clearFile(String file) {
        try {
            FileChannel fc = new FileOutputStream(file).getChannel();
            fc.write(ByteBuffer.wrap("".getBytes()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
