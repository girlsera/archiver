package IO;

import java.io.File;
import java.io.FileInputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.*;

public class ReadFile {
    public static String read(String file) {
        try {
            FileChannel fc = new FileInputStream(file).getChannel();
            ByteBuffer bb = ByteBuffer.allocate((int) fc.size());
            fc.read(bb);
            bb.flip();
            return Charset.forName("UTF-8").decode(bb).toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static Map<String, String> readTable(String file) {
        String text = read(file);
        Map<String, String> map = new HashMap<>();
        if (text.length() > 0) {
            List<String> list = Arrays.asList(text.split("\r\n"));
            list.stream().forEach(i -> {
                if (i.contains("end")) map.put(i.substring(0, 3), i.substring(4, i.length()));
                else map.put(i.substring(0, 1), i.substring(2, i.length()));
            });
        }
        return map;
    }

    public static List<Byte> readBytes(String fileName) {
        List<Byte> bytes = new ArrayList<>();
        try {
            File file = new File(fileName);
            FileInputStream fin = new FileInputStream(file);
            byte[] b = new byte[(int) file.length()];
            int i = -1;
            fin.read(b);
            for (int j = 0; j < b.length; j++) {
                bytes.add(b[j]);
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return bytes;
    }
}
