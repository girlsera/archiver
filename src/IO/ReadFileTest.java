package IO;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;


import static org.junit.Assert.*;

public class ReadFileTest {

    @Ignore
    @Test
    public void read() throws Exception {
        assertTrue(ReadFile.read("src/output.txt").indexOf("ext") != -1);
    }

    @Test
    public void readTable() throws Exception {
        Assert.assertEquals("000000000000001", ReadFile.readTable("src/output.txt").get('㿃'));
    }

}