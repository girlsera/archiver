package IO;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author Elina Batyrova
 */
public class WriteFileTest {

    @Test
    public void writeToFileTest() {
        String expecteds = "Text" + "\r\n";
        WriteFile.writeIntoFile("example.txt", "Text");
        Assert.assertEquals(ReadFile.read("example.txt"), expecteds);
    }
}
